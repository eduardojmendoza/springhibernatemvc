package com.journaldev.spring;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonArray;
import com.journaldev.spring.model.Country;
import com.journaldev.spring.model.Person;
import com.journaldev.spring.service.CountryService;
import com.journaldev.spring.service.PersonService;

@Controller
public class PersonController {
	
	
	private PersonService personService;
	private CountryService countryService;
	
	@Autowired(required=true)
	@Qualifier(value="personService")
	public void setPersonService(PersonService ps){
		this.personService = ps;
	}
	
	@Autowired(required=true)
	@Qualifier(value="countryService")
	public void setCountryService(CountryService cs){
		this.countryService = cs;
	}
	
	@RequestMapping(value = "/persons", method = RequestMethod.GET)
	public String listPersons(Model model) {
		
		model.addAttribute("person", new Person());
		model.addAttribute("country", -1);
		model.addAttribute("listPersons", this.personService.listPersons());
		model.addAttribute("listCountries",  this.countryService.listCountries());
		return "person";
	}
	
	@RequestMapping(value = "/jsonp/{id}")
	@ResponseBody
    public Person person(@PathVariable("id") int id) {
        return this.personService.getPersonById(id);
    }
	
	@ResponseBody
	@RequestMapping(value = "/json")
    public String json() {
        return "Json";
    }
	
	
	//For add and update person both
	@RequestMapping(value= "/person/add", method = RequestMethod.POST)
	public String addPerson(@ModelAttribute("person") Person p){
		
		if(p.getId() == 0){
			//new person, add it
			this.personService.addPerson(p);
		}else{
			//existing person, call update
			this.personService.updatePerson(p);
		}
		
		return "redirect:/persons";
		
	}
	
	@RequestMapping("/remove/{id}")
    public String removePerson(@PathVariable("id") int id){
		
        this.personService.removePerson(id);
        return "redirect:/persons";
    }
 
    @RequestMapping("/edit/{id}")
    public String editPerson(@PathVariable("id") int id, Model model){
        model.addAttribute("person", this.personService.getPersonById(id));
        model.addAttribute("country", this.personService.getPersonById(id).getCountry().getId());
        model.addAttribute("listPersons", this.personService.listPersons());
        model.addAttribute("listCountries",this.countryService.listCountries());
        return "person";
    }
    
   /* @RequestMapping(value = "/personsCountry",method = RequestMethod.GET)
    public @ResponseBody JsonArray getPersonsFromCountry() {
    	//System.out.println("VUELTA: "+this.personService.getPersonsFromCountry("").toString());
    	return this.personService.getPersonsFromCountry("");
    }*/
	
}
