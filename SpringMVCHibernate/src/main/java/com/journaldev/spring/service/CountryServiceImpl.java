package com.journaldev.spring.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.journaldev.spring.dao.CountryDAO;
import com.journaldev.spring.model.Country;


@Service
public class CountryServiceImpl implements CountryService{

	private CountryDAO countryDAO;
	
	public void setCountryDAO(CountryDAO countryDAO) {
		this.countryDAO=countryDAO;
	}
	
	@Override
	@Transactional
	public void addCountry(Country c) {
		this.countryDAO.addCountry(c);
	}
	

	@Override
	@Transactional
	public void updateCountry(Country c) {
		this.countryDAO.updateCountry(c);
	}

	@Override
	@Transactional
	public List<Country> listCountries() {
		return this.countryDAO.listCountries();
	}

	@Override
	@Transactional
	public Country getCountryById(int id) {
		return this.countryDAO.getCountryById(id);
	}

	@Override
	@Transactional
	public void removeCountry(int id) {
		this.countryDAO.removeCountry(id);
	}

}
