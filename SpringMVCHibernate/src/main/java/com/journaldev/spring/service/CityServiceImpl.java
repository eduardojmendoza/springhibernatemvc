package com.journaldev.spring.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.journaldev.spring.dao.CityDAO;
import com.journaldev.spring.model.City;


@Service("cityService")
public class CityServiceImpl implements CityService{

	@Autowired
	@Qualifier(value="cityDAO")
	private CityDAO cityDAO;
	
	
	public void setCityDAO(CityDAO cityDAO) {
		this.cityDAO=cityDAO;
	}
	
	@Autowired
	@Qualifier(value="cityDAO2")
	private CityDAO cityDAO2;
	
	
	public void setCityDAO2(CityDAO cityDAO) {
		this.cityDAO=cityDAO;
	}
	
	@Override
	@Transactional
	public void addCity(City c) {
		this.cityDAO.addCity(c);
	}
	

	@Override
	@Transactional
	public void updateCity(City c) {
		this.cityDAO.updateCity(c);
	}

	@Override
	@Transactional
	public List<City> listCities(String name) {
		return this.cityDAO.listCities(name);
	}

	@Override
	@Transactional
	public City getCityById(int id) {
		return this.cityDAO.getCityById(id);
	}

	@Override
	@Transactional
	public void removeCity(int id) {
		this.cityDAO.removeCity(id);
	}

	@Override
	@Transactional
	public City getCityByName(String name) {
		return this.cityDAO2.getCityByName(name);
	}

}
