package com.journaldev.spring;

import java.awt.Component;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import org.hibernate.engine.spi.EntityUniqueKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.journaldev.spring.model.Country;
import com.journaldev.spring.service.CountryService;


@Controller
public class CountryController {
	
	
	private CountryService countryService;
	
	@Autowired(required=true)
	@Qualifier(value="countryService")
	public void setCountryService(CountryService cs){
		this.countryService = cs;
	}
	
	@RequestMapping(value = "/country", method = RequestMethod.GET)
	public String listCountries(Model model) {
		model.addAttribute("country", new Country());
		model.addAttribute("listCountries", this.countryService.listCountries());
		return "country";
	} 
	
	@RequestMapping(value = "/country/add", method = RequestMethod.POST)
	public String addCountry(@ModelAttribute("country") Country c) {
		if (c.getId()==0) {
			try {
				this.countryService.addCountry(c);
				Object frame = null;
				JOptionPane.showMessageDialog((Component) frame, "Coutry was added!");
			} catch (Exception e) {
				Object frame = null;
				JOptionPane.showMessageDialog((Component) frame, "Country already exists!");
			}
			
		}else {
			this.countryService.updateCountry(c);
			Object frame = null;
			JOptionPane.showMessageDialog((Component) frame, "Coutry was updated!");
		}
		return "redirect:/country";
	}
	
	@RequestMapping(value="/removeCountry/{id}")
	public String removeCountry(@PathVariable("id") int id) throws SQLException{
		Object frame = null;
		try {
			this.countryService.removeCountry(id);
			JOptionPane.showMessageDialog((Component) frame, "Coutry was removed!");
		} catch (Exception e) {
			JOptionPane.showMessageDialog((Component) frame, "Country can not be removed, it is being used by a person!");
		}
		return "redirect:/country";
	}
	
	@RequestMapping(value="/editCountry/{id}")
	public String editCountry(@PathVariable("id") int id,Model model) {
		model.addAttribute("country",this.countryService.getCountryById(id));
		model.addAttribute("listCountries",this.countryService.listCountries());
		return "country";
	}
	
	
	
	
	
	
	
}