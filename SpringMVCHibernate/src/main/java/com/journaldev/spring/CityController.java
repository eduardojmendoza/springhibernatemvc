package com.journaldev.spring;

import java.awt.Component;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.swing.JOptionPane;

import org.hibernate.engine.spi.EntityUniqueKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.journaldev.spring.model.City;
import com.journaldev.spring.service.CityService;


@Controller
public class CityController {
	
	
	private CityService cityService;
	
	@Autowired(required=true)
	@Qualifier(value="cityService")
	public void setCityService(CityService cs){
		this.cityService = cs;
	}
	
	@RequestMapping(value = "/city", method = RequestMethod.GET)
	public String listCities(Model model) {
		String city=null;
		model.addAttribute("city", new City());
		model.addAttribute("listCities", this.cityService.listCities(city));
		return "city";
	} 
	
	@RequestMapping(value = "/city/add", method = RequestMethod.POST)
	public String addCity(@ModelAttribute("city") City c) {
		if (c.getId()==0) {
			try {
				this.cityService.addCity(c);
				Object frame = null;
				JOptionPane.showMessageDialog((Component) frame, "City was added!");
			} catch (Exception e) {
				Object frame = null;
				JOptionPane.showMessageDialog((Component) frame, "City already exists!");
			}
			
		}else {
			this.cityService.updateCity(c);
			Object frame = null;
			JOptionPane.showMessageDialog((Component) frame, "City was updated!");
		}
		return "redirect:/city";
	}
	
	@RequestMapping(value="/removeCity/{id}")
	public String removeCity(@PathVariable("id") int id) throws SQLException{
		Object frame = null;
		try {
			this.cityService.removeCity(id);
			JOptionPane.showMessageDialog((Component) frame, "City was removed!");
		} catch (Exception e) {
			JOptionPane.showMessageDialog((Component) frame, "City can not be removed, it is being used by a person!");
		}
		return "redirect:/city";
	}
	
	@RequestMapping(value="/editCity/{id}")
	public String editCity(@PathVariable("id") int id,Model model) {
		String city=null;
		model.addAttribute("city",this.cityService.getCityById(id));
		model.addAttribute("listCities",this.cityService.listCities(city));
		return "city";
	}
	
	@RequestMapping(value="/city/found", method = RequestMethod.GET)
	public String editCityByName(Model model,HttpServletRequest req) throws SQLException {
		try {
			String name = req.getParameter("name");
			City city=this.cityService.getCityByName(name);
			if (city instanceof City) {
				model.addAttribute("city",city);
			}else{
				model.addAttribute("city", new City());
			}
			model.addAttribute("listCities",this.cityService.listCities(name));
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "city";
		
	}
	
}