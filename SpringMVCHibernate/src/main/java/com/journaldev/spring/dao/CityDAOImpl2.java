package com.journaldev.spring.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.journaldev.spring.model.City;

@Repository("cityDAO2")
public class CityDAOImpl2 implements CityDAO{

	private static final Logger logger = LoggerFactory.getLogger(CityDAOImpl2.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		// TODO Auto-generated method stub
	}

	@Override
	public void addCity(City c) {
		// TODO Auto-generated method stub
	}

	@Override
	public void updateCity(City c) {
		// TODO Auto-generated method stub
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<City> listCities(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public City getCityById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeCity(int id) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public City getCityByName(String name) {
		Session session = this.sessionFactory.getCurrentSession();
		List listaCities=session.createQuery("from City C WHERE C.name ='"+name+"'").list();
		if (listaCities.size()>0) {
			return (City) listaCities.get(0);
		}else {
			return null;
		}
	}
}
