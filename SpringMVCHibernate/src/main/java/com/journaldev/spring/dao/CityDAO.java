package com.journaldev.spring.dao;

import java.util.List;

import com.journaldev.spring.model.City;

public interface CityDAO {

	public void addCity(City c);
	public void updateCity(City c);
	public List<City> listCities(String name);
	public City getCityById(int id);
	public City getCityByName(String name);
	public void removeCity(int id);
}
