package com.journaldev.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.journaldev.spring.model.City;

@Repository("cityDAO")
public class CityDAOImpl implements CityDAO{

	private static final Logger logger = LoggerFactory.getLogger(CityDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void addCity(City c) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(c);
		logger.info("City saved successfully, Person Details="+c);
	}

	@Override
	public void updateCity(City c) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(c);
		logger.info("City updated successfully, City Details="+c);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<City> listCities(String name) {
		Session session = this.sessionFactory.getCurrentSession();
		List<City> countriesList =null;
		
		if (name==null) {
			countriesList = session.createQuery("from City").list();
		}else {
			countriesList = session.createQuery("from City C WHERE C.name like '%"+name+"%'").list();
		}
		for(City c : countriesList){
			logger.info("City List::"+c);
		}
		return countriesList;
	}

	@Override
	public City getCityById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		City c = (City) session.load(City.class, new Integer(id));
		logger.info("City loaded successfully, City details="+c);
		return c;
	}

	@Override
	public void removeCity(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		City c = (City) session.load(City.class, new Integer(id));
		if(null != c){
			session.delete(c);
		}
		logger.info("City deleted successfully, person details="+c);
	}

	@Override
	public City getCityByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
