package com.journaldev.spring.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.journaldev.spring.model.Country;
import com.journaldev.spring.model.Person;

@Repository
public class PersonDAOImpl implements PersonDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(PersonDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void addPerson(Person p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(p);
		logger.info("Person saved successfully, Person Details="+p);
	}

	@Override
	public void updatePerson(Person p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(p);
		logger.info("Person updated successfully, Person Details="+p);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Person> listPersons() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Person> personsList = session.createQuery("from Person").list();
		for(Person p : personsList){
			logger.info("Person List::"+p);
		}
		return personsList;
	}

	@Override
	public Person getPersonById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		Person p = (Person) session.load(Person.class, new Integer(id));
		logger.info("Person loaded successfully, Person details="+p);
		return p;
	}

	@Override
	public void removePerson(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Person p = (Person) session.load(Person.class, new Integer(id));
		if(null != p){
			session.delete(p);
		}
		logger.info("Person deleted successfully, person details="+p);
	}

	
	
	@Override
	public JsonArray getPersonsFromCountry() {
	
		Session session = this.sessionFactory.getCurrentSession();		
		
		Query query = session.createQuery("from Country c, Person p WHERE c.id = p.id_country_fk ");
				
		//Query query = session.createQuery("from Country c INNER JOIN Person p ON c.id = p.id_country_fk ");
		//query.setParameter("id_country", 1);
		List list = query.list();		
		//session.close(); 
		Gson gson=new Gson();
		JsonArray jsonArrayPersonsAndCountry=new JsonArray();
		for (int i=0;i<list.size();i++) {
		 
			 Object[] row = (Object[]) list.get(i);
			 Country country = (Country)row[0];
			 Person person = (Person)row[1];
				 	
			 String jsonPerson=gson.toJson(person);
		     String jsonCountry=gson.toJson(person);
		     JsonObject jsonPersonAndCountry=new JsonObject();
		     jsonPersonAndCountry.addProperty("person", jsonPerson);
		     jsonPersonAndCountry.addProperty("country", jsonCountry);
		     jsonArrayPersonsAndCountry.add(jsonPersonAndCountry);			
        }
		
        
        return jsonArrayPersonsAndCountry;
	}
}