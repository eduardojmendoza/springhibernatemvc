package com.journaldev.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.journaldev.spring.model.Country;

@Repository
public class CountryDAOImpl implements CountryDAO{

	private static final Logger logger = LoggerFactory.getLogger(CountryDAOImpl.class);
	
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void addCountry(Country c) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(c);
		logger.info("Country saved successfully, Person Details="+c);
	}

	@Override
	public void updateCountry(Country c) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(c);
		logger.info("Country updated successfully, Country Details="+c);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Country> listCountries() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Country> countriesList = session.createQuery("from Country").list();
		for(Country c : countriesList){
			logger.info("Country List::"+c);
		}
		return countriesList;
	}

	@Override
	public Country getCountryById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		Country c = (Country) session.load(Country.class, new Integer(id));
		logger.info("Country loaded successfully, Country details="+c);
		return c;
	}

	@Override
	public void removeCountry(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Country c = (Country) session.load(Country.class, new Integer(id));
		if(null != c){
			session.delete(c);
		}
		logger.info("Country deleted successfully, person details="+c);
	}

}
